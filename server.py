import socket
import threading
from datetime import datetime

# Constants
HEADER = 64 # First message to the server from client
PORT = 5050 # Localhost port
SERVER = socket.gethostbyname(socket.gethostname())  # Make public ip for online
# Get server ip automatically
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"
# LOG = open(f'logs/{datetime.today()}.txt', 'w')
LOG = open('log.txt', 'w')

CLIENTS = []

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Create socket and specify type (AF_INET) over internet
server.bind(ADDR)
# Bind socket to address

def receive(conn, addr): # Function to receive messages from clients
    msg_length = conn.recv(HEADER).decode(FORMAT) # Decode to string
    # Tells us how long the message that is coming is
    if msg_length: # Check for valid message first
        msg_length = int(msg_length)
        # Convert to int
        msg = conn.recv(msg_length).decode(FORMAT)
        # Message receives message length in bytes

        return msg

def handle_client(conn, addr): # Handle communications between client and sever
    username = receive(conn, addr) # First message is username
    LOG.write(f"[NEW CONNECTION] {addr} '{username}' connected.\n")
    print(f"[NEW CONNECTION] {addr} '{username}' connected.")
    conn.send("Username set.".encode(FORMAT))

    CLIENTS.append(conn) # Add to clients list
    for x in CLIENTS: print(x)

    connected = True
    while connected:
        msg = receive(conn, addr)

        if msg == DISCONNECT_MESSAGE:
            connected = False

        LOG.write(f"[{addr}] {username}: {msg}\n") # Log message
        print(f"[{addr}] {username}: {msg}") # Print message
        conn.send("Msg received".encode(FORMAT)) # Send to client

    conn.close() # Close connection
    LOG.write(f"[CLOSED] Client closed connection {addr} '{username}'\n")
    print(f"[CLOSED] Client closed connection {addr} '{username}'")

def start(): # Listen for connections
    server.listen()
    print(f"[LISTENING] Server is listening on {SERVER}")
    while True:
        conn, addr = server.accept() # Store port and ip addr on new connection
        thread = threading.Thread(target = handle_client, args = (conn, addr))
        # Create thread and send to handle client with args
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.active_count() - 1}") # Always 1 thread running so -1
    

LOG.write("[STARTING] Server has starting...\n")
print("[STARTING] Server is starting...")
start()