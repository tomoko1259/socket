import socket
import sys

# Constants
HEADER = 64
PORT = 5050
SERVER = socket.gethostbyname(socket.gethostname()) 
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"
ADDR = (SERVER, PORT)

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# client.connect(ADDR)
# Connect client to server

# Fist message sent is the length of the message to come
def send(msg):
    message = msg.encode(FORMAT) # Encode the string to byte object
    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT) 
    send_length += b' ' * (HEADER - len(send_length)) # Pad the length to equal the header size
    client.send(send_length)
    client.send(message)
    print(client.recv(2048).decode(FORMAT)) # Receive message from server
    # If not received then error in server

def start():
    client.connect(ADDR)
    print("[CONNECTED] Connected to server, to disconnect type '!DISCONNECT'")

    sys.stdout.write("Username: ") # Set clients custom username
    username = input()
    send(username)

    text = ""
    while text != DISCONNECT_MESSAGE:
        text = input()
        send(text)

start()
# send("Hello World!")
# input()
# send("Hello POGGERS!")
# input()
# send(DISCONNECT_MESSAGE)
